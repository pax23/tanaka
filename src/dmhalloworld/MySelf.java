package dmhalloworld;

public class MySelf {
	
	private int age;
	private String firstname;
	private String surname;
	private String goal;
	private String University;

	
	
	public MySelf() {
		age = 21;
		firstname = "Tanaka";
		surname = "Mapaike";
		goal = "Helpful and Notifiable Developments in IT";
		University = "Midlands State University";

		
	}
	/**
	 * @return the age
	 */
	public int getAge() {
		
		return age;

	}

	/**
	 * @return the firstname
	 */
	public String getFirstname() {
		
		return firstname;
	}
	/**
	 * @return the goal
	 */
	public String getGoal() {
		
		return goal;
	}
	/**
	 * @param the surname to set
	 */
	public String getSurname() {

		return surname;
	}

	/**
	 * @return the Univerity
	 */
	public String University() {

		return University;

	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MySelf [age=" + age + ", firstname=" + firstname + ", surname=" + surname + ", goal=" + goal + ",University=" + University"]";
	}
	

}
